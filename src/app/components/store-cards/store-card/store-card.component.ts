import { Component, Input, OnInit } from '@angular/core';
import { StoreModel } from '../../../models/store.model';

@Component({
  selector: 'nc-store-card',
  templateUrl: './store-card.component.html',
  styleUrls: ['./store-card.component.scss']
})
export class StoreCardComponent implements OnInit {

  @Input()
  store: StoreModel;

  constructor() { }

  ngOnInit(): void {
  }

}
