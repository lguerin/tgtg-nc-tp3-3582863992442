import { Component, Input, OnInit } from '@angular/core';
import { StoreModel } from '../../models/store.model';

@Component({
  selector: 'nc-store-cards',
  templateUrl: './store-cards.component.html',
  styleUrls: ['./store-cards.component.scss']
})
export class StoreCardsComponent implements OnInit {

  @Input()
  stores: Array<StoreModel> = [];

  constructor() { }

  ngOnInit(): void {
  }

}
